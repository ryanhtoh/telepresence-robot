# From 1/7/2020
![](image.jpg)

This robot uses an Arduino for motor control and an ESP32 for the camera and web-hosting. The robot can be access and controlled from anywhere with an internet connection.

esp32Cam_WebRobot.ino is the main file for the ESP32. It is uploaded to an esp32-cam via an FTDI Serial Adapter via a modified Arduino IDE.
app_httpd.cpp is a modified file from Espressif Systems, for stream functions. Some like face recognition do not seem to work.
camera_pins.h just defines the pins.

camera_index.h has the byte array for the HTML/javascript webpage for controlling the robot as well as streaming video. The associated html code is in index.html. 
Use the following CyberChef settings for converting between html and hex: https://gchq.github.io/CyberChef/#recipe=Find_/_Replace(%7B'option':'Regex','string':','%7D,'',true,false,false,false)Remove_whitespace(true,false,true,false,false,false)From_Hex('0x')Gunzip()

arduino_MotorControl.ino is the main file for the Arduino. It controls the robot's motors, and communicates with the ESP32 via a serial connection.
