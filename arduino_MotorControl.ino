/*
Main file for the Arduino. It controls the robot's motors, and communicates with the ESP32 via a serial connection.
Tx Out from ESP32 to Rx0 on Arduino
Need common Ground
*/

int curDir = -1; // current direction
// define pins
int motor1Pin1 = 4;
int motor1Pin2 = 5;
int motor2Pin1 = 2;
int motor2Pin2 = 3;
int motor1EN = 6;
int motor2EN = 9;
char theDir = ' ';

void setup() {
  Serial.begin(115200); // esp32 communications speed
  
  pinMode(motor1Pin1, OUTPUT);
  pinMode(motor1Pin2, OUTPUT);
  pinMode(motor2Pin1, OUTPUT);
  pinMode(motor2Pin2, OUTPUT);
  pinMode(motor1EN, OUTPUT);
  pinMode(motor2EN, OUTPUT);
  digitalWrite(motor1Pin1, LOW);
  digitalWrite(motor1Pin2, LOW);
  digitalWrite(motor2Pin1, LOW);
  digitalWrite(motor2Pin2, LOW);
  analogWrite(motor1EN, 255);
  analogWrite(motor2EN, 255);
}

void forward(int pin){
  digitalWrite(pin, LOW);
  digitalWrite(pin+1,HIGH);
}

void motorStop(int pin){
  digitalWrite(pin, LOW);
  digitalWrite(pin+1,LOW);
}

void backward(int pin){
  digitalWrite(pin, HIGH);
  digitalWrite(pin+1,LOW);
}

void loop() {
  if (Serial.available() > 0) { // if a keystroke received from ESP32
    curDir = Serial.read(); // read the byte
    // Control robot based on byte, via WASD movement
    switch (curDir) {
      case 83: // s = Reverse
        analogWrite(motor1EN, 255);
        analogWrite(motor2EN, 255);
        forward(2);
        backward(4);
        break;
      case 32: // space = STOP
        analogWrite(motor1EN, 255);
        analogWrite(motor2EN, 255);
        motorStop(2);
        motorStop(4);
        break;
      case 87: // w = Forward
        analogWrite(motor1EN, 255);
        analogWrite(motor2EN, 255);
        backward(2);
        forward(4);
        break;
      case 68: // d = Right
        analogWrite(motor1EN, 255);
        analogWrite(motor2EN, 255);
        forward(2);
        forward(4);
        break;
      case 65: // a = Left
        analogWrite(motor1EN, 255);
        analogWrite(motor2EN, 255);
        backward(2);
        backward(4);
        break;
      default: // STOP
        analogWrite(motor1EN, 255);
        analogWrite(motor2EN, 255);
        motorStop(2);
        motorStop(4);
        break;
    }
  }
}
